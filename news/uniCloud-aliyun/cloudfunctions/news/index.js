'use strict';
const db = uniCloud.database();

exports.main = async (event, context) => {
        const collection = db.collection('news');
        // 获取符合条件的新闻总数
        let total = await collection.where({category_id: event.category_id }).count();
        // 根据分页参数获取对应页的新闻列表
        let start = (event.currentPage - 1) * event.pageSize;
        let res = await collection.where({ category_id: event.category_id })
                                  .orderBy('date', 'desc')
                                  .skip(start)
                                  .limit(event.pageSize)
                                  .get();
        
        // 返回结果给前端
        return {
            total: total.total,
            list: res.data
        }
};
