'use strict';
const db = uniCloud.database();

exports.main = async (event, context) => {
    if (event.type == 'get') {
        return await db.collection('category').get();
    }
    // 调用获取分类菜单的函数
    const tabs = await getCategoryMenu();
    // 返回分类菜单数据
    return tabs;
};

// 定义获取分类菜单的异步函数
async function getCategoryMenu() {
    try {
        // 调用云函数获取分类菜单数据
        let res = await uniCloud.callFunction({
            name: 'category',
            data: { type: 'get' }
        });
        // 返回分类菜单数据
        return res.result.data;
    } catch (err) {
        // 错误处理
        console.error('Error while fetching category menu:', err);
        // 如果出现错误，可以返回一个空数组或者其他默认值
        return [];
    }
}
